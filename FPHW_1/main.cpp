#include <iostream>
#include <io.h>
#include <windows.h>
#include <ctime>
#include <fstream>
#include <tchar.h>
#include <clocale>

using namespace std;

bool changed = false;

typedef basic_string<TCHAR> tString;

void createLogFile(string dest) {

	time_t currentTime = time(0);
	struct tm tstruct;

	char buf[MAX_PATH];

	ofstream fp;
	TCHAR pathBuf[MAX_PATH];
	tstruct = *localtime(&currentTime);

	GetCurrentDirectory(MAX_PATH, pathBuf);

	fp.open("Update.log", ios::app);

	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
	fp << dest.c_str() << " has been updated at " << buf << endl;

	cout << "Log File created at current folder\n\t";
	wcout << pathBuf << endl;
	fp.close();
}


bool isNotExist(string dest) {
	struct _finddata_t fd;
	long handle;

	handle = _findfirst(dest.c_str(), &fd);

	if (handle == -1) {

		return true;
	}
	else return false;
}


bool checkUpdate(string src, string dest) {
	struct _finddata_t fd_src;
	struct _finddata_t fd_dest;
	long handle_src;
	long handle_dest;

	handle_dest = _findfirst(dest.c_str(), &fd_dest);
	handle_src = _findfirst(src.c_str(), &fd_src);



	time_t time_src = fd_src.time_write;
	time_t time_dest = fd_dest.time_write;
	//cout << "SRC ACCESS TIME: " << time_src << endl;
	//cout << "DEST ACCESS TIME: " << time_dest << endl;

	if (difftime(time_src, time_dest) > 0) {
		cout << endl;

		return true;
	}

	return false;

}


void makeDir(string dest) {
	wchar_t destWCHAR[MAX_PATH];
	mbstowcs(destWCHAR, dest.c_str(), dest.length() + 1);
	CreateDirectory(destWCHAR, NULL);
	
}


void copyFile(string src, string dest) {

	wchar_t srcWCHAR[MAX_PATH];
	wchar_t destWCHAR[MAX_PATH];

	mbstowcs(srcWCHAR, src.c_str(), src.length() + 1);
	mbstowcs(destWCHAR, dest.c_str(), dest.length() + 1);

	int result = CopyFile(srcWCHAR, destWCHAR, 0);

	if (!result) {
		_tprintf(TEXT("\n\n\t\tsrc: %ws\n"), srcWCHAR);
		_tprintf(TEXT("\t\tdest: %ws\n"), destWCHAR);
		cerr << "Copying files Error code: " << GetLastError() << endl;
		if (GetLastError() == 2) {
			cout << "Are there any \"Not English\" path..?\n\n" << endl;
		}
	}

	else {
		
		createLogFile(dest);
		changed = true;
		cout << dest.c_str() << " **Updated**" << endl;
	}


}

void intoDirectory(string srcDir, string destDir) {

	long handle;
	struct _finddata_t fd;
	int result;
	string curDirFiles = srcDir + string("\\*");


	handle = _findfirst(curDirFiles.c_str(), &fd);

	if (handle == -1)
		return;

	result = _findnext(handle, &fd);

	do {
		if (fd.attrib == _A_SUBDIR) { // File is a Directory
			if ((strcmp(fd.name, ".") != 0 && strcmp(fd.name, "..") != 0)) {

				string nextDir = srcDir + string("\\") + fd.name;

				string src = (srcDir + string("\\") + string(fd.name));
				string dest = (destDir + string("\\") + string(fd.name));
				if (/*checkUpdate(src, dest) || */isNotExist(dest))
					makeDir(dest);

				intoDirectory(nextDir, (destDir + "\\" + fd.name));

			}
		}
		else {
			// File is not Directory

			string src = (srcDir + string("\\") + string(fd.name));
			string dest = (destDir + string("\\") + string(fd.name));
			if (checkUpdate(src, dest) || isNotExist(dest))
				copyFile(src, dest);

		}
		result = _findnext(handle, &fd);
	} while (result != -1);

	_findclose(handle);

}

int main(int argc, char** argv) {

	string srcDir, destDir;
	if (argc != 3) {
		cerr << "\nUsage: " << argv[0] << " " << "<src directory> <dest directory>" << endl;
		return -1;
	}

	cout << "Source Dir:\t" << argv[1] << endl;
	cout << "Dest Dir:\t" << argv[2] << endl;

	srcDir = string(argv[1]);
	destDir = string(argv[2]);

	makeDir(destDir);
	intoDirectory(srcDir, destDir);


	if (changed)
		cout << "\n\nUpdated." << endl;
	else
		cout << "\n\nNothing updated." << endl;

	return 0;

}